# Testing branching on bitbucket with codeship

Had some issues, here's a testing repo

To reproduce:

Setup the app locally

```
rails new codeship_branch_test
cd codeship_branch_test
rails g scaffold post title:string content:text
rake db:migrate
# Test and see that it works
rake

```

Create branches

```
git branch development
git checkout development
git push --set-upstream origin development
git branch staging
git checkout staging
git push --set-upstream origin staging

```

Do some changes and push/merge/push (Doing this multiple times result in the same...)

```
git checkout development
echo "test" > "test.txt"
git add test.txt
git commit -m "test"
git push
git checkout staging
git merge development
git push
```

I was expecting that the push to staging branch would trigger a build, as that is setup to do deployment, while development branch is "edge".

